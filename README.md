# Primeiro Projeto de Tecnicas de Programação 1


# Objetivo

Este projeto tem o objetivo de segmentar o conhecimento de programação orientada a objetos por meio dos materiais de aulas de laboratório do professor Victor Prisacariu da Universidade de Oxford.
O projeto é separado em 4 seções e cada uma usa simulações físicas de objetos para exemplificar as relações entre as classes dentro do programa.

## Bouncing Ball

- Bouncing ball simula o movimento de uma bola contida em uma região bidimencional [-1, 1] X [-1, 1] que é abandonada no ponto (0, 0) e que cai sob o efeito da gravidade.

- O projeto consiste nos aquivos ball.h e simulation.h, cabeçalhos contendo declarações de classe, e ball.cpp e test-ball.cpp, que implementam as definições dos cabeçalhos.

- Compila-se o projeto fazendo gcc ball.cpp test-ball.cpp -o ball.

- A bola é instanciada pelo construtor com os seguintes valores para parâmetros: 
     r = 0.1, x = 0 , y = 0, vx = 0.3, vy = -0.1, g = 9.8, m = 1,xmin = -1, xmax = 1, ymin = 1, ymax = 1.
- A função display imprime as coordenadas x e y da bola separadas por um espaço, de forma que saída do programa consiste no conjunto de coordenadas da posição da bola em 100 iterações de tempo.

- O diagrama de classes uml é o seguinte:

![Ball uml](/ball_uml.png)

- A saída do programa é a seguinte:

```
0.01 -0.00877778
0.02 -0.0284444
0.03 -0.059
0.04 -0.100444
0.05 -0.152778
0.06 -0.216
0.07 -0.290111
0.08 -0.375111
0.09 -0.471
0.1 -0.577778
0.11 -0.695444
0.12 -0.824
0.13 -0.824
0.14 -0.695444
0.15 -0.577778
0.16 -0.471
0.17 -0.375111
0.18 -0.290111
0.19 -0.216
0.2 -0.152778
0.21 -0.100444
0.22 -0.059
0.23 -0.0284444
0.24 -0.00877778
0.25 -8.1532e-17
0.26 -0.00211111
0.27 -0.0151111
0.28 -0.039
0.29 -0.0737778
0.3 -0.119444
0.31 -0.176
0.32 -0.243444
0.33 -0.321778
0.34 -0.411
0.35 -0.511111
0.36 -0.622111
0.37 -0.744
0.38 -0.876778
0.39 -0.876778
0.4 -0.744
0.41 -0.622111
0.42 -0.511111
0.43 -0.411
0.44 -0.321778
0.45 -0.243444
0.46 -0.176
0.47 -0.119444
0.48 -0.0737778
0.49 -0.039
0.5 -0.0151111
0.51 -0.00211111
0.52 -4.90059e-16
0.53 -0.00877778
0.54 -0.0284444
0.55 -0.059
0.56 -0.100444
0.57 -0.152778
0.58 -0.216
0.59 -0.290111
0.6 -0.375111
0.61 -0.471
0.62 -0.577778
0.63 -0.695444
0.64 -0.824
0.65 -0.824
0.66 -0.695444
0.67 -0.577778
0.68 -0.471
0.69 -0.375111
0.7 -0.290111
0.71 -0.216
0.72 -0.152778
0.73 -0.100444
0.74 -0.059
0.75 -0.0284444
0.76 -0.00877778
0.77 -7.47666e-16
0.78 -0.00211111
0.79 -0.0151111
0.8 -0.039
0.81 -0.0737778
0.82 -0.119444
0.83 -0.176
0.84 -0.243444
0.85 -0.321778
0.86 -0.411
0.87 -0.511111
0.88 -0.622111
0.89 -0.744
0.89 -0.876778
0.88 -0.876778
0.87 -0.744
0.86 -0.622111
0.85 -0.511111
0.84 -0.411
0.83 -0.321778
0.82 -0.243444
0.81 -0.176
0.8 -0.119444
0.79 -0.0737778
```


## Springmass

- Spring mass simula um sistema massa-mola, em que que duas circunferencias são unidas por uma mola.

- De forma semelhante ao projeto anterior, este consiste nos cabeçalhos springmass.h, simulation.h e nas implementações springmass.cpp (implementa a classe mola, classe massa e a classe massa-mola) e test-springmass.cpp.

- Compila-se o projeto fazendo gcc springmass.cpp test-springmass.cpp -o springmass.

- A seguinte saída do programa é a posição x e y de cada uma das duas circunferências, o comprimento da mola e a energia total do sistema (cinética e potencial elástica).
 

```
(-0.499444,-0.0009)(0.499444,-0.0009) spring length: 0.998889 energy: 0.160011
(-0.497798,-0.0036)(0.497798,-0.0036) spring length: 0.995595 energy: 0.160014
(-0.495123,-0.0081)(0.495123,-0.0081) spring length: 0.990246 energy: 0.160021
(-0.49153,-0.0144)(0.49153,-0.0144) spring length: 0.98306 energy: 0.160033
(-0.48717,-0.0225)(0.48717,-0.0225) spring length: 0.974341 energy: 0.16005
(-0.482231,-0.0324)(0.482231,-0.0324) spring length: 0.964462 energy: 0.160071
(-0.476927,-0.0441)(0.476927,-0.0441) spring length: 0.953854 energy: 0.160095
(-0.47149,-0.0576)(0.47149,-0.0576) spring length: 0.942979 energy: 0.160118
(-0.46616,-0.0729)(0.46616,-0.0729) spring length: 0.93232 energy: 0.16014
(-0.461176,-0.09)(0.461176,-0.09) spring length: 0.922352 energy: 0.160159
(-0.456762,-0.1089)(0.456762,-0.1089) spring length: 0.913524 energy: 0.160173
(-0.453119,-0.1296)(0.453119,-0.1296) spring length: 0.906239 energy: 0.160181
(-0.450417,-0.1521)(0.450417,-0.1521) spring length: 0.900834 energy: 0.160185
(-0.448783,-0.1764)(0.448783,-0.1764) spring length: 0.897566 energy: 0.160186
(-0.4483,-0.2025)(0.4483,-0.2025) spring length: 0.8966 energy: 0.160186
(-0.448999,-0.2304)(0.448999,-0.2304) spring length: 0.897998 energy: 0.160187
(-0.45086,-0.2601)(0.45086,-0.2601) spring length: 0.90172 energy: 0.160191
(-0.45381,-0.2916)(0.45381,-0.2916) spring length: 0.90762 energy: 0.1602
(-0.457729,-0.3249)(0.457729,-0.3249) spring length: 0.915457 energy: 0.160214
(-0.462449,-0.36)(0.462449,-0.36) spring length: 0.924899 energy: 0.160234
(-0.46777,-0.3969)(0.46777,-0.3969) spring length: 0.93554 energy: 0.160258
(-0.473459,-0.4356)(0.473459,-0.4356) spring length: 0.946918 energy: 0.160285
(-0.479267,-0.4761)(0.479267,-0.4761) spring length: 0.958535 energy: 0.160312
(-0.484938,-0.5184)(0.484938,-0.5184) spring length: 0.969875 energy: 0.160337
(-0.490217,-0.5625)(0.490217,-0.5625) spring length: 0.980433 energy: 0.160358
(-0.494866,-0.6084)(0.494866,-0.6084) spring length: 0.989732 energy: 0.160373
(-0.498674,-0.6561)(0.498674,-0.6561) spring length: 0.997348 energy: 0.160382
(-0.501464,-0.7056)(0.501464,-0.7056) spring length: 1.00293 energy: 0.160387
(-0.503102,-0.7569)(0.503102,-0.7569) spring length: 1.0062 energy: 0.160388
(-0.503506,-0.81)(0.503506,-0.81) spring length: 1.00701 energy: 0.160388
(-0.502646,-0.8649)(0.502646,-0.8649) spring length: 1.00529 energy: 0.160389
(-0.500551,-0.9216)(0.500551,-0.9216) spring length: 1.0011 energy: 0.160394
(-0.4973,-0.9216)(0.4973,-0.9216) spring length: 0.994601 energy: 0.160404
(-0.49303,-0.8649)(0.49303,-0.8649) spring length: 0.986061 energy: 0.160421
(-0.487921,-0.81)(0.487921,-0.81) spring length: 0.975842 energy: 0.160445
(-0.482192,-0.7569)(0.482192,-0.7569) spring length: 0.964384 energy: 0.160473
(-0.476092,-0.7056)(0.476092,-0.7056) spring length: 0.952184 energy: 0.160503
(-0.46989,-0.6561)(0.46989,-0.6561) spring length: 0.93978 energy: 0.160534
(-0.463859,-0.6084)(0.463859,-0.6084) spring length: 0.927719 energy: 0.160562
(-0.45827,-0.5625)(0.45827,-0.5625) spring length: 0.916541 energy: 0.160585
(-0.453375,-0.5184)(0.453375,-0.5184) spring length: 0.906751 energy: 0.160602
(-0.449398,-0.4761)(0.449398,-0.4761) spring length: 0.898796 energy: 0.160612
(-0.446523,-0.4356)(0.446523,-0.4356) spring length: 0.893046 energy: 0.160616
(-0.444888,-0.3969)(0.444888,-0.3969) spring length: 0.889776 energy: 0.160617
(-0.444577,-0.36)(0.444577,-0.36) spring length: 0.889154 energy: 0.160617
(-0.445615,-0.3249)(0.445615,-0.3249) spring length: 0.891231 energy: 0.160619
(-0.447969,-0.2916)(0.447969,-0.2916) spring length: 0.895938 energy: 0.160625
(-0.451545,-0.2601)(0.451545,-0.2601) spring length: 0.903089 energy: 0.160638
(-0.456195,-0.2304)(0.456195,-0.2304) spring length: 0.912389 energy: 0.160658
(-0.461722,-0.2025)(0.461722,-0.2025) spring length: 0.923444 energy: 0.160685
(-0.467888,-0.1764)(0.467888,-0.1764) spring length: 0.935777 energy: 0.160717
(-0.474426,-0.1521)(0.474426,-0.1521) spring length: 0.948851 energy: 0.160752
(-0.481047,-0.1296)(0.481047,-0.1296) spring length: 0.962093 energy: 0.160787
(-0.487458,-0.1089)(0.487458,-0.1089) spring length: 0.974915 energy: 0.160819
(-0.493372,-0.09)(0.493372,-0.09) spring length: 0.986744 energy: 0.160844
(-0.498522,-0.0729)(0.498522,-0.0729) spring length: 0.997045 energy: 0.160863
(-0.502673,-0.0576)(0.502673,-0.0576) spring length: 1.00535 energy: 0.160874
(-0.505631,-0.0441)(0.505631,-0.0441) spring length: 1.01126 energy: 0.160878
(-0.507254,-0.0324)(0.507254,-0.0324) spring length: 1.01451 energy: 0.160879
(-0.507457,-0.0225)(0.507457,-0.0225) spring length: 1.01491 energy: 0.160879
(-0.50622,-0.0144)(0.50622,-0.0144) spring length: 1.01244 energy: 0.160881
(-0.503584,-0.0081)(0.503584,-0.0081) spring length: 1.00717 energy: 0.160889
(-0.499655,-0.0036)(0.499655,-0.0036) spring length: 0.99931 energy: 0.160904
(-0.494595,-0.0009)(0.494595,-0.0009) spring length: 0.989189 energy: 0.160928
(-0.488618,-5.38198e-16)(0.488618,-5.38198e-16) spring length: 0.977237 energy: 0.160959
(-0.481984,-0.0009)(0.481984,-0.0009) spring length: 0.963968 energy: 0.160997
(-0.47498,-0.0036)(0.47498,-0.0036) spring length: 0.94996 energy: 0.161037
(-0.467915,-0.0081)(0.467915,-0.0081) spring length: 0.93583 energy: 0.161076
(-0.461102,-0.0144)(0.461102,-0.0144) spring length: 0.922204 energy: 0.161112
(-0.454846,-0.0225)(0.454846,-0.0225) spring length: 0.909692 energy: 0.16114
(-0.44943,-0.0324)(0.44943,-0.0324) spring length: 0.89886 energy: 0.161161
(-0.445102,-0.0441)(0.445102,-0.0441) spring length: 0.890205 energy: 0.161173
(-0.442065,-0.0576)(0.442065,-0.0576) spring length: 0.88413 energy: 0.161178
(-0.440465,-0.0729)(0.440465,-0.0729) spring length: 0.880929 energy: 0.161178
(-0.440385,-0.09)(0.440385,-0.09) spring length: 0.88077 energy: 0.161178
(-0.441843,-0.1089)(0.441843,-0.1089) spring length: 0.883686 energy: 0.161181
(-0.444787,-0.1296)(0.444787,-0.1296) spring length: 0.889575 energy: 0.161191
(-0.449101,-0.1521)(0.449101,-0.1521) spring length: 0.898202 energy: 0.161209
(-0.454604,-0.1764)(0.454604,-0.1764) spring length: 0.909208 energy: 0.161237
(-0.461062,-0.2025)(0.461062,-0.2025) spring length: 0.922124 energy: 0.161273
(-0.468197,-0.2304)(0.468197,-0.2304) spring length: 0.936395 energy: 0.161316
(-0.475698,-0.2601)(0.475698,-0.2601) spring length: 0.951397 energy: 0.161362
(-0.483235,-0.2916)(0.483235,-0.2916) spring length: 0.96647 energy: 0.161407
(-0.490473,-0.3249)(0.490473,-0.3249) spring length: 0.980945 energy: 0.161447
(-0.497087,-0.36)(0.497087,-0.36) spring length: 0.994174 energy: 0.161479
(-0.502778,-0.3969)(0.502778,-0.3969) spring length: 1.00556 energy: 0.161501
(-0.507286,-0.4356)(0.507286,-0.4356) spring length: 1.01457 energy: 0.161514
(-0.510398,-0.4761)(0.510398,-0.4761) spring length: 1.0208 energy: 0.161519
(-0.511965,-0.5184)(0.511965,-0.5184) spring length: 1.02393 energy: 0.161519
(-0.511903,-0.5625)(0.511903,-0.5625) spring length: 1.02381 energy: 0.161519
(-0.510201,-0.6084)(0.510201,-0.6084) spring length: 1.0204 energy: 0.161523
(-0.506918,-0.6561)(0.506918,-0.6561) spring length: 1.01384 energy: 0.161535
(-0.502188,-0.7056)(0.502188,-0.7056) spring length: 1.00438 energy: 0.161557
(-0.496208,-0.7569)(0.496208,-0.7569) spring length: 0.992416 energy: 0.161589
(-0.489232,-0.81)(0.489232,-0.81) spring length: 0.978463 energy: 0.161632
(-0.481561,-0.8649)(0.481561,-0.8649) spring length: 0.963122 energy: 0.161681
(-0.47353,-0.9216)(0.47353,-0.9216) spring length: 0.947061 energy: 0.161734
(-0.465494,-0.9216)(0.465494,-0.9216) spring length: 0.930988 energy: 0.161785
(-0.457808,-0.8649)(0.457808,-0.8649) spring length: 0.915617 energy: 0.161829
(-0.450819,-0.81)(0.450819,-0.81) spring length: 0.901637 energy: 0.161865

```