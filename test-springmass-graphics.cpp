/** file: test-springmass-graphics.cpp
** brief: Tests the spring mass simulation with graphics
** author: Andrea Vedaldi
**/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>
using namespace std;

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
	/* ---------------------------------------------------------------- */
{
private:
	Figure figure;

public:
	SpringMassDrawable()
		: figure("SpringMass System")
	{
		figure.addDrawable(this);
	}

	void draw() {
		int i = 0;
		double r;
		double energy = getEnergy();
		Mass *m1, *m2;
		Vector2 v1, v2;

		while (i < masses.size()) {
			r = masses[i].getRadius();
			v1 = masses[i].getPosition();
			figure.drawCircle(v1.x, v1.y, r);
			i++;
		}
		i = 0;
		while (i < springs.size()) {
			m1 = springs[i].getMass1();
			v1 = m1->getPosition();
			m2 = springs[i].getMass2();
			v2 = m2->getPosition();
			figure.drawLine(v1.x, v1.y, v2.x, v2.y, 2);
			i++;
		}
		ostringstream strs;
		strs << energy;
		string str = strs.str();
		figure.drawString(-0.9, 0.7, "Energia");
		figure.drawString(-0.9, 0.5, str);
	}

	void display() {
		figure.update();
	}
};

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	SpringMassDrawable springmass;
	const double mass = 0.05;
	const double radius = 0.06;
	const double naturalLength = 0.4;

	Mass m1(Vector2(-.5, 0), Vector2(), mass, radius);
	Mass m2(Vector2(+.5, 0), Vector2(), mass, radius);

	springmass.pushMass(m1);
	springmass.pushMass(m2);
	springmass.pushSpring(0, 1, -0.05, naturalLength, 1);

	run(&springmass, 1.0 / 60);
	return 0;
}
